# multiAgents.py
# --------------
# Licensing Information: Please do not distribute or publish solutions to this
# project. You are free to use and extend these projects for educational
# purposes. The Pacman AI projects were developed at UC Berkeley, primarily by
# John DeNero (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# For more info, see http://inst.eecs.berkeley.edu/~cs188/sp09/pacman.html

from util import manhattanDistance
from game import Directions
import random, util

from game import Agent

class ReflexAgent(Agent):
  """
    A reflex agent chooses an action at each choice point by examining
    its alternatives via a state evaluation function.

    The code below is provided as a guide.  You are welcome to change
    it in any way you see fit, so long as you don't touch our method
    headers.
  """


  def getAction(self, gameState):
    """
    You do not need to change this method, but you're welcome to.

    getAction chooses among the best options according to the evaluation function.

    Just like in the previous project, getAction takes a GameState and returns
    some Directions.X for some X in the set {North, South, West, East, Stop}
    """
    # Collect legal moves and successor states
    legalMoves = gameState.getLegalActions()
   # print gameState
    # Choose one of the best actions
    scores = [self.evaluationFunction(gameState, action) for action in legalMoves]
    bestScore = max(scores)
    bestIndices = [index for index in range(len(scores)) if scores[index] == bestScore]
    chosenIndex = random.choice(bestIndices) # Pick randomly among the best

    "Add more of your code here if you want to"

    return legalMoves[chosenIndex]

  def evaluationFunction(self, currentGameState, action):
    """
    Design a better evaluation function here.

    The evaluation function takes in the current and proposed successor
    GameStates (pacman.py) and returns a number, where higher numbers are better.

    The code below extracts some useful information from the state, like the
    remaining food (newFood) and Pacman position after moving (newPos).
    newScaredTimes holds the number of moves that each ghost will remain
    scared because of Pacman having eaten a power pellet.

    Print out these variables to see what you're getting, then combine them
    to create a masterful evaluation function.
    """
    # Useful information you can extract from a GameState (pacman.py)
    successorGameState = currentGameState.generatePacmanSuccessor(action)
    newPos = successorGameState.getPacmanPosition()
    newFood = successorGameState.getFood()
    newGhostStates = successorGameState.getGhostStates()
    newScaredTimes = [ghostState.scaredTimer for ghostState in newGhostStates]
    bonus = 0
    newFoodList = newFood.asList()
    if len(currentGameState.getFood().asList()) > len(newFoodList):
        bonus =2
    '''
    print ' S game state',successorGameState
    print 'Ghost state',newGhostStates[0].getPosition()
    print 'Scared time',newScaredTimes
    print 'Food list',newFood.asList()
    print 'Pacman pos:',newPos
    print 'Current pos:',currentGameState.getPacmanPosition()
    '''
    #newFoodList = newFood.asList()
    score = 0
    if len(newFoodList) == 0:
        return 100
    minDis = min([manhattanDistance(newPos,food) for food in newFoodList])
    foodFactor = float(1)/minDis
    #print 'minfood',float(1)/minDis
    score += foodFactor
    
    distanceToGhost = min([manhattanDistance(newPos,ghost.getPosition()) for ghost in newGhostStates])
    #print 'dtg',distanceToGhost
    if distanceToGhost < 3:
        if distanceToGhost == 0:
            return -100
        else:
            score += distanceToGhost
        
    '''
    ratio = float(minDis)/distanceToGhost 
    if ( ratio >= 2 ):
        score += ratio*foodFactor + distanceToGhost
    else:
        score += foodFactor + (float(1)/ratio)*distanceToGhost
    '''
    #score += min([manhattanDistance(newPos,ghostPos.getPosition()) for ghostPos in newGhostStates])
    
    "*** YOUR CODE HERE ***"
    #print 'score',score
    return score + bonus
    return successorGameState.getScore()

def scoreEvaluationFunction(currentGameState):
  """
    This default evaluation function just returns the score of the state.
    The score is the same one displayed in the Pacman GUI.

    This evaluation function is meant for use with adversarial search agents
    (not reflex agents).
  """
  return currentGameState.getScore()

class MultiAgentSearchAgent(Agent):
  """
    This class provides some common elements to all of your
    multi-agent searchers.  Any methods defined here will be available
    to the MinimaxPacmanAgent, AlphaBetaPacmanAgent & ExpectimaxPacmanAgent.

    You *do not* need to make any changes here, but you can if you want to
    add functionality to all your adversarial search agents.  Please do not
    remove anything, however.

    Note: this is an abstract class: one that should not be instantiated.  It's
    only partially specified, and designed to be extended.  Agent (game.py)
    is another abstract class.
  """

  def __init__(self, evalFn = 'scoreEvaluationFunction', depth = '2'):
    self.index = 0 # Pacman is always agent index 0
    self.evaluationFunction = util.lookup(evalFn, globals())
    self.depth = int(depth)
    self.lastActions = 2*[None]


class MinimaxAgent(MultiAgentSearchAgent):
  """
    Your minimax agent (question 2)
  """

  def getAction(self, gameState):
    """
      Returns the minimax action from the current gameState using self.depth
      and self.evaluationFunction.

      Here are some method calls that might be useful when implementing minimax.

      gameState.getLegalActions(agentIndex):
        Returns a list of legal actions for an agent
        agentIndex=0 means Pacman, ghosts are >= 1

      Directions.STOP:
        The stop direction, which is always legal

      gameState.generateSuccessor(agentIndex, action):
        Returns the successor game state after an agent takes an action

      gameState.getNumAgents():
        Returns the total number of agents in the game
    """
    "*** YOUR CODE HERE ***"
    numberOfAgents = gameState.getNumAgents()
    actionList = numberOfAgents*(self.depth+1)*[None]
    def maxValue( state, agentIndex, Agentdepth, alpha, beta):
        v = -99999
        actions = state.getLegalActions( agentIndex)
        values = []
        for action in actions:
            successor = state.generateSuccessor( agentIndex, action)
            newValue = value( successor, agentIndex+1, Agentdepth, alpha, beta)
            if v < newValue and action!= Directions.STOP:
                actionList[numberOfAgents*Agentdepth + agentIndex] = action
                v = newValue
            if v >= beta:
                return v
            alpha = max( alpha, v)
        
        return v

    def minValue( state, agentIndex, Agentdepth, alpha, beta):
        v = 99999
        actions = state.getLegalActions( agentIndex)
        for action in actions:
            successor = state.generateSuccessor( agentIndex, action)
            newValue = value( successor, agentIndex+1, Agentdepth, alpha, beta)
            if v > newValue and action!=Directions.STOP:
                actionList[numberOfAgents*Agentdepth + agentIndex] = action
                v = newValue
            if v <= alpha:
                return v
            beta = min( beta, v)
        return v

    def value( state, agentIndex, Agentdepth, alpha, beta):
        #print agentIndex, Agentdepth
        if  Agentdepth == self.depth or state.isLose() or state.isWin():
            return self.evaluationFunction( state)
        if agentIndex == numberOfAgents:
            Agentdepth += 1
            agentIndex = 0
        if  agentIndex == 0:
            return maxValue( state, agentIndex, Agentdepth, alpha, beta)
        else:
            return minValue( state, agentIndex, Agentdepth, alpha, beta)
    val = value( gameState, 0, 0, -10000, 10000)
    return actionList[0]
    util.raiseNotDefined()

class AlphaBetaAgent(MultiAgentSearchAgent):
  """
    Your minimax agent with alpha-beta pruning (question 3)
  """

  def getAction(self, gameState):
    """
      Returns the minimax action using self.depth and self.evaluationFunction
    """
    "*** YOUR CODE HERE ***"
    numberOfAgents = gameState.getNumAgents()
    actionList = numberOfAgents*(self.depth+1)*[None]
    def maxValue( state, agentIndex, Agentdepth, alpha, beta):
        v = -99999
        actions = state.getLegalActions( agentIndex)
        values = []
        for action in actions:
            successor = state.generateSuccessor( agentIndex, action)
            newValue = value( successor, agentIndex+1, Agentdepth, alpha, beta)
            if v < newValue and action!= Directions.STOP:
                actionList[numberOfAgents*Agentdepth + agentIndex] = action
                v = newValue
            if v >= beta:
                return v
            alpha = max( alpha, v)
        
        return v

    def minValue( state, agentIndex, Agentdepth, alpha, beta):
        v = 99999
        actions = state.getLegalActions( agentIndex)
        for action in actions:
            successor = state.generateSuccessor( agentIndex, action)
            newValue = value( successor, agentIndex+1, Agentdepth, alpha, beta)
            if v > newValue and action!=Directions.STOP:
                actionList[numberOfAgents*Agentdepth + agentIndex] = action
                v = newValue
            if v <= alpha:
                return v
            beta = min( beta, v)
        return v

    def value( state, agentIndex, Agentdepth, alpha, beta):
        #print agentIndex, Agentdepth
        if  Agentdepth == self.depth or state.isLose() or state.isWin():
            return self.evaluationFunction( state)
        if agentIndex == numberOfAgents:
            Agentdepth += 1
            agentIndex = 0
        if  agentIndex == 0:
            return maxValue( state, agentIndex, Agentdepth, alpha, beta)
        else:
            return minValue( state, agentIndex, Agentdepth, alpha, beta)
    val = value( gameState, 0, 0, -10000, 10000)
    return actionList[0]
 
    util.raiseNotDefined()

class ExpectimaxAgent(MultiAgentSearchAgent):
  """
    Your expectimax agent (question 4)
  """

  def getAction(self, gameState):
    """
      Returns the expectimax action using self.depth and self.evaluationFunction

      All ghosts should be modeled as choosing uniformly at random from their
      legal moves.
    """
    "*** YOUR CODE HERE ***"
    numberOfAgents = gameState.getNumAgents()
    actionList = numberOfAgents*(self.depth+1)*[None]
    def maxValue( state, agentIndex, Agentdepth):
        v = -99999
        actions = state.getLegalActions( agentIndex)
        values = []
        for action in actions:
            if action != Directions.STOP:
                successor = state.generateSuccessor( agentIndex, action)
                newValue = value( successor, agentIndex+1, Agentdepth)
                if v < newValue:
                    actionList[numberOfAgents*Agentdepth + agentIndex] = action
                    v = newValue
        return v

    def expValue( state, agentIndex, Agentdepth):
        v = 0
        actions = state.getLegalActions( agentIndex)
        p = float(1)/len(actions)
        for action in actions:
            if action!= Directions.STOP:
                successor = state.generateSuccessor( agentIndex, action)
                newValue = value( successor, agentIndex+1, Agentdepth)
                actionList[numberOfAgents*Agentdepth + agentIndex] = action
                v += p*newValue
        return v

    def value( state, agentIndex, Agentdepth):
        #print agentIndex, Agentdepth
        if  Agentdepth == self.depth or state.isLose() or state.isWin():
            return self.evaluationFunction( state)
        if agentIndex == numberOfAgents:
            Agentdepth += 1
            agentIndex = 0
        if  agentIndex == 0:
            return maxValue( state, agentIndex, Agentdepth)
        else:
            return expValue( state, agentIndex, Agentdepth)
    val = value( gameState, 0, 0)
    return actionList[0]
 

    util.raiseNotDefined()

def betterEvaluationFunction(currentGameState):
  """
    Your extreme ghost-hunting, pellet-nabbing, food-gobbling, unstoppable
    evaluation function (question 5).

    DESCRIPTION: <write something here so we know what you did>
  """
  "*** YOUR CODE HERE ***"
  #successorGameState = currentGameState.generatePacmanSuccessor(action)
  newPos = currentGameState.getPacmanPosition()
  newFood = currentGameState.getFood()
  newGhostStates = currentGameState.getGhostStates()
  newScaredTimes = min([ghostState.scaredTimer for ghostState in newGhostStates])
  bonus = 0
  newFoodList = newFood.asList()
  capsuleList = currentGameState.getCapsules()
  if  len(newFoodList) == 0:
      return 100
  foodDistance = min([manhattanDistance(food,newPos) for food in newFoodList])
  #score = float(1)/foodDistance
  #foodDistances.sort( lambda x,y: int(y[1] - x[1]))
  ghostDistances = [(ghost, manhattanDistance( ghost.getPosition(), newPos)) for ghost in newGhostStates]
  ghostDistances.sort( lambda x,y: int(x[1] - y[1]))
  capsuleScore = 0
  if( len( capsuleList) != 0):
      nearestCapsuleDistance = min([manhattanDistance( capsule, newPos) for capsule in capsuleList])
      if nearestCapsuleDistance == 0:
          bonus = 2
          capsuleScore = 2
      elif nearestCapsuleDistance < 3:
          capsuleScore = float(1)/nearestCapsuleDistance
      else:
          capsuleScore = 0
  #nearestFood, distanceToFood = foodDistances[0]
  nearestGhost, distanceToGhost = ghostDistances[0]
  foodScore =  float(1)/foodDistance
  ghostScore = 0
  
  if distanceToGhost == 0:
      return -100
  else:
      ghostScore -= float(1)/distanceToGhost
  if nearestGhost.scaredTimer > distanceToGhost:
      ghostScore = 1 + ghostScore 

  score = foodScore + ghostScore + capsuleScore + float(50)/len(newFoodList) 
  return score

  util.raiseNotDefined()

# Abbreviation
better = betterEvaluationFunction

class ContestAgent(MultiAgentSearchAgent):
  """
    Your agent for the mini-contest
  """

  def getAction(self, gameState):
    """
      Returns an action.  You can use any method you want and search to any depth you want.
      Just remember that the mini-contest is timed, so you have to trade off speed and computation.

      Ghosts don't behave randomly anymore, but they aren't perfect either -- they'll usually
      just make a beeline straight towards Pacman (or away from him if they're scared!)
    """
    "*** YOUR CODE HERE ***"
    util.raiseNotDefined()

